
# Reddit Scraper

Reddit Scraper is a Python-based tool for scraping user data, posts, and comments from Reddit. It provides functionalities to collect information about Reddit users, including their date joined, karma, active subreddits, and engagement metrics for their last 50 posts and comments.

## Project Structure



- **collected_data**: Contains templates for overview documents, generated Word documents (`<username>_overview.docx`), and CSV files (`<username>_scraping_results.csv`).
- **database**: Directory for storing the SQLite database (`reddit_users.db`).
- **database.py**: Module for handling database operations, including creating tables and inserting user data.
- **helpers.py**: Contains helper functions for generating overview documents and saving scraping results to CSV files.
- **reddit_api.py**: Module for making HTTP requests to Reddit's API endpoints.
- **reddit_scraper.py**: Main script for initiating the scraping process.
- **requirements.txt**: List of dependencies required by the project.


## Installation

### Prerequisites

- **Python**: Ensure that Python is installed on your system. You can download and install Python from the [official website](https://www.python.org/).

To set up the project, follow these steps:

1. Clone the repository:
```
git clone https://gitlab.com/laptevna97/web-scrapping.git
```
2. Create a virtual environment and activate it (Optional but Recommended)
3. Install dependencies:
```
pip install -r requirements.txt
```


## Detailed Explanation of the Process

The Reddit Scraper project employs a systematic approach to gather user data, posts, and comments from Reddit. The scraping process involves the following steps:

1. **Fetching User Information**: The scraping begins by fetching basic user information from Reddit's API using the `requests` library in Python. Information such as date joined, username, user avatar, and Reddit karma is obtained from the `/user/{username}/about.json` endpoint.

2. **Retrieving Posts and Comments**: After fetching user information, the scraper proceeds to retrieve the user's last 50 posts and comments. This is achieved by making HTTP requests to the `/user/{username}/submitted.json` and `/user/{username}/comments.json` endpoints, respectively, also utilizing the `requests` library.

3. **Handling Edge Cases**: The scraping solution is designed to handle various edge cases, such as users with private profiles, new users with no posts or comments, and users with deleted posts or comments. Error handling mechanisms are implemented to gracefully handle exceptions and ensure the robustness of the scraper.

4. **Data Processing and Storage**: Once the data is fetched, it undergoes processing and is structured into a standardized format. User information, posts, and comments are organized into separate data structures for further analysis and storage. The collected data is stored in multiple formats to facilitate accessibility and analysis:

   - **Relational Database**: User data, posts, and comments are stored in a SQLite database (`reddit_users.db`) using the `database.py` module. 

   - **CSV Files**: The scraping results are exported to CSV files (`<username>_scraping_results.csv`) using the `save_to_csv` function in the `helpers.py` module. This tabular format allows for easy analysis using spreadsheet software.

   - **Word Documents**: An overview document (`<username>_overview.docx`) is generated using the `generate_overview_document` function in the `helpers.py` module. This document provides a comprehensive summary of the scraping results in a formatted layout suitable for presentation or documentation purposes.


Overall, the scraping process is orchestrated using a combination of HTTP requests, data processing techniques, and storage mechanisms, facilitated by Python libraries such as `requests`, `docxtpl`, and `csv`.
## Data Modeling

The collected data from Reddit is structured into distinct categories to facilitate organization and analysis. The data model encompasses the following components:

### User Information
- **Date Joined**: The date when the user joined Reddit.
- **Username**: The unique username of the Reddit user.
- **User Avatar**: The URL of the user's avatar (profile picture).
- **Reddit Karma**: The cumulative karma (combined link karma and comment karma) of the user.
- **Subreddits the user is active in**: A list of subreddits in which the user has participated or posted content.

### Last 50 Posts
- **Subreddit**: The subreddit where the post was submitted.
- **Title**: The title of the post.
- **Upvotes**: The number of upvotes received by the post.
- **Downvotes**: The number of downvotes received by the post.
- **Number of Comments**: The total number of comments on the post.
- **Media**: Optional media content associated with the post, such as image or video URLs.

### Last 50 Comments
- **Subreddit**: The subreddit where the comment was posted.
- **Comment Text**: The text content of the comment.
- **Upvotes**: The number of upvotes received by the comment.
- **Downvotes**: The number of downvotes received by the comment.


## High-level Description of the Technical Solution

The scraping solution is implemented in Python, utilizing the requests library for making HTTP requests to Reddit's API endpoints. The code is organized into functions for fetching user information, posts, and comments. The DocxTemplate library is used for generating overview documents based on provided templates.

## Generating Word Document

The `generate_overview_document` function in `helpers.py` generates an overview document describing the scraping results. It populates a Word document template with the scraped data and saves it to the `collected_data` folder.

## Creating CSV File

The `save_to_csv` function in `helpers.py` saves the scraping results to a CSV file. It writes the data to a CSV file using the `csv` module and saves it to the `collected_data` folder.

## Inserting User Data into the Database

The `insert_user_data` function in `database.py` inserts user data into the SQLite database. It connects to the database, executes an SQL INSERT statement, and commits the transaction.

## Running the Code

To run the Reddit Scraper, execute the `reddit_scraper.py` script from the reddit_scraper folder:


```
python reddit_scraper/reddit_scraper.py
```



The script will initiate the scraping process for predefined Reddit usernames specified within the `usernames` list in the `reddit_scraper.py` file. You can modify this list to include the usernames you want to scrape.

### Output

The scraping results will be saved as CSV files, overview documents, and inserted into the database. CSV files and overview documents will be located in the `collected_data` folder within the project directory, while the data will also be stored in the SQLite database whic is located in `database` folder.
## Test Plan

To ensure the reliability and accuracy of the Reddit Scraper, a comprehensive testing strategy is employed. The following test plan outlines various scenarios and test cases to validate the functionality of the scraping solution:

### 1. User Data Retrieval Tests

- **Test Case 1:** Verify that user information is correctly fetched from Reddit's API.
  - Input: Valid Reddit username
  - Expected Output: User information retrieved successfully
  
- **Test Case 2:** Test scenarios include users with public profiles, private profiles, and non-existent profiles.
  - Input: Various Reddit usernames with different profile settings
  - Expected Output: Handling of private profiles, user not found, etc.

### 2. Post and Comment Retrieval Tests

- **Test Case 1:** Ensure that the last 50 posts and comments are retrieved accurately for each user.
  - Input: Reddit usernames with known posting/commenting history
  - Expected Output: Correct retrieval and formatting of post/comment data
  
- **Test Case 2:** Test edge cases such as users with no posts or comments, users with fewer than 50 posts/comments, and users with deleted posts/comments.
  - Input: Reddit usernames with specific posting/commenting scenarios
  - Expected Output: Proper handling of edge cases

### 3. Database Insertion Tests

- **Test Case 1:** Validate that user data is inserted correctly into the database.
  - Input: Sample user data
  - Expected Output: Successful insertion with accurate data
  
- **Test Case 2:** Test cases cover scenarios such as successful insertion, duplicate usernames, and invalid data formats.
  - Input: Various scenarios for database insertion
  - Expected Output: Handling of insertion errors and data integrity checks

### 4. CSV Export Tests

- **Test Case 1:** Check that scraping results are exported correctly to CSV files.
  - Input: Sample scraping results
  - Expected Output: Properly formatted CSV files with accurate data
  
- **Test Case 2:** Verify the CSV file format and data integrity.
  - Input: Various scenarios for CSV export
  - Expected Output: Valid CSV files without data loss or corruption

### 5. Document Generation Tests

- **Test Case 1:** Confirm that overview documents are generated accurately.
  - Input: Sample scraping results
  - Expected Output: Word documents with correctly formatted data
  
- **Test Case 2:** Test the completeness and correctness of the generated documents.
  - Input: Various scenarios for document generation
  - Expected Output: Error-free documents with all required data included

### 6. Error Handling and Exception Tests

- **Test Case 1:** Test the handling of HTTP errors, timeouts, and other exceptions.
  - Input: Simulated HTTP errors, timeouts, etc.
  - Expected Output: Proper error messages and graceful handling of exceptions
  
- **Test Case 2:** Verify that appropriate error messages are displayed, and the program gracefully handles errors without crashing.
  - Input: Faulty API responses, network errors, etc.
  - Expected Output: Clear error messages and continued program execution

### 7. Integration Tests

- **Test Case 1:** Perform end-to-end tests to validate the entire scraping process.
  - Input: Multiple users with varying profiles and posting/commenting histories
  - Expected Output: Successful scraping for all users without errors or data loss

### Libraries and Tools
- **Requests Library**: The `requests` library is employed for making HTTP requests to Reddit's API endpoints. It facilitates easy retrieval of user information, posts, and comments.
- **SQLite Database**: The `sqlite3` module is utilized to interact with an SQLite database for storing and querying scraped data. The database provides a structured and efficient storage mechanism.
- **DocxTemplate Library**: The `docxtpl` library is utilized to generate overview documents in Word format based on predefined templates. This allows for the creation of professional-looking reports summarizing the scraping results.
- **CSV Module**: The built-in `csv` module is used for handling CSV files, enabling the export of scraping results to a tabular format for analysis and sharing.

### Modular Code Structure
- **Modular Design**: The codebase is organized into separate modules, each responsible for specific tasks such as data retrieval, database operations, document generation, and overall orchestration of the scraping process. This modular design promotes code readability, maintainability, and reusability.
- **Function-based Approach**: Functions are utilized extensively to encapsulate distinct functionalities, making the codebase modular, testable, and easier to debug.

### Error Handling and Logging
- **Error Handling**: Comprehensive error handling is implemented to gracefully handle exceptions such as HTTP errors, timeouts, and invalid responses from the Reddit API. This ensures the stability and robustness of the scraping process.
- **Logging**: The `logging` module is employed to log informative messages and error traces, aiding in troubleshooting and debugging during development and deployment.

### Automation and Scalability
- **Script Execution**: The main `reddit_scraper.py` script serves as the entry point for initiating the scraping process. It orchestrates the retrieval of data for multiple Reddit users, enabling batch processing and automation of the scraping tasks.
- **Scalability**: The solution is designed to scale with the addition of new functionalities or expansion to scrape data from other sources beyond Reddit. The modular architecture and use of Python facilitate future enhancements and extensions.




