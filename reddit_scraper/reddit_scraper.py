import os
from datetime import datetime, timezone
from time import sleep
from requests.exceptions import HTTPError, Timeout

from database import insert_user_data, create_database
from helpers import save_to_csv, generate_overview_document
from reddit_api import call

current_dir = os.path.dirname(os.path.abspath(__file__))
output_folder = os.path.join(current_dir, "collected_data")
database_file = os.path.join(output_folder, "reddit_users.db")


def fetch_user_info(username: str):
    """
    Fetches basic information about the Reddit user.
    """
    endpoint_about = f"user/{username}/about.json"
    response_about = call("GET", endpoint=endpoint_about, headers={"User-Agent": "RedditScraper"})
    if response_about.status_code == 404:
        print(f"User '{username}' not found.")
        return None

    user_data = response_about.json().get("data")

    user_subreddits = set()
    endpoint_submitted = f"user/{username}/submitted.json"
    response_submitted = call("GET", endpoint=endpoint_submitted, headers={"User-Agent": "RedditScraper"})
    submitted_data = response_submitted.json().get("data", {}).get("children", [])
    for post in submitted_data:
        subreddit = post.get("data", {}).get("subreddit")
        user_subreddits.add(subreddit)

    user_info = {
        "Date Joined": datetime.fromtimestamp(user_data.get("created_utc", 0), timezone.utc).strftime(
            '%Y-%m-%d %H:%M:%S'),
        "Username": user_data.get("name", "Information not available"),
        "User Avatar": user_data.get("icon_img", "Information not available"),
        "Reddit Karma": user_data.get("link_karma", 0) + user_data.get("comment_karma", 0),
        "Subreddits the user is active in": list(user_subreddits),
    }
    return user_info


def fetch_last_50_posts(username: str) -> list:
    """
    Fetches the last 50 posts submitted by the Reddit user.
    """
    endpoint_submitted = f"user/{username}/submitted.json"
    response_submitted = call("GET", endpoint=endpoint_submitted, headers={"User-Agent": "RedditScraper"})
    submitted_data = response_submitted.json().get("data", {}).get("children", [])

    last_50_posts = []
    for post in submitted_data[:50]:
        post_data = post.get("data", {})
        media_url = None
        if post_data.get("is_reddit_media_domain"):
            media_url = post_data.get("url")
        elif post_data.get("post_hint") == "image":
            media_url = post_data.get("url")
        elif "preview" in post_data:
            preview = post_data.get("preview", {})
            if isinstance(preview.get("images"), list) and preview["images"]:
                first_image_data = preview["images"][0] if len(preview["images"]) > 0 else {}
                if isinstance(first_image_data, dict):
                    image_data = first_image_data.get("source", {})
                    if isinstance(image_data, dict) and "url" in image_data:
                        media_url = image_data["url"]

        post_info = {
            "Subreddit": post_data.get("subreddit", "Unknown"),
            "Title": post_data.get("title", "Unknown"),
            "Upvotes": post_data.get("ups", 0),
            "Downvotes": post_data.get("downs", 0),
            "Number of Comments": post_data.get("num_comments", 0),
            "Media": media_url,
        }
        last_50_posts.append(post_info)

    return last_50_posts


def fetch_last_50_comments(username: str) -> list:
    """
    Fetches the last 50 comments made by the Reddit user.
    """
    endpoint_comments = f"user/{username}/comments.json"
    response_comments = call("GET", endpoint=endpoint_comments, headers={"User-Agent": "RedditScraper"})
    comments_data = response_comments.json().get("data", {}).get("children", [])

    last_50_comments = []
    for comment in comments_data[:50]:
        comment_data = comment["data"]
        comment_info = {
            "Subreddit": comment_data.get("subreddit", "Unknown"),
            "Comment Text": comment_data.get("body", "Unknown"),
            "Upvotes": comment_data.get("ups", 0),
            "Downvotes": comment_data.get("downs", 0),
        }
        last_50_comments.append(comment_info)

    return last_50_comments


def calculate_average_engagement(posts: list, comments: list) -> dict:
    """
    Calculates the average engagement (upvotes + downvotes) for posts and comments.
    """
    total_post_engagement = sum(post["Upvotes"] + post["Downvotes"] for post in posts)
    total_comment_engagement = sum(comment["Upvotes"] + comment["Downvotes"] for comment in comments)

    average_post_engagement = total_post_engagement / len(posts) if len(posts) > 0 else 0
    average_comment_engagement = total_comment_engagement / len(comments) if len(comments) > 0 else 0

    return {
        "Average Post Engagement": average_post_engagement,
        "Average Comment Engagement": average_comment_engagement,
    }


def scrape_reddit_user(username: str):
    """
    Scrapes Reddit user information, posts, comments, and calculates average engagement.
    """
    user_info = fetch_user_info(username)
    if user_info is None:
        # Skip scraping if user info is not available
        return

    try:
        last_50_posts = fetch_last_50_posts(username)
        last_50_comments = fetch_last_50_comments(username)
        average_engagement = calculate_average_engagement(last_50_posts, last_50_comments)
        user_info.update({"Last 50 Posts": last_50_posts, "Last 50 Comments": last_50_comments})
        user_info.update(average_engagement)

        # If you dont want/want to save scraping results to the CSV file -> comment/uncomment next line

        insert_user_data(user_info)

        save_to_csv(user_info, username)
        generate_overview_document(user_info, username)

        print(f"Scraping for {username} user completed and results saved successfully.")

    except HTTPError as http_err:
        if http_err.response.status_code == 404:
            print(f"User '{username}' not found.")
        else:
            print(f"HTTP error occurred: {http_err}")
    except Timeout:
        print("Timeout occurred while making the request. Please check your network connection.")
    except Exception as e:
        print(f"Error: {e}")


def main():
    """
    Main function to initiate scraping for multiple users.
    """
    create_database()  # Ensure database and table are created
    usernames = ["TOBTThrowAway", "TalosAI", "Ok-Judgment-1181"]  # List of usernames to scrape
    for username in usernames:
        scrape_reddit_user(username)
        print('*' * 40)
        sleep(6)  # Add a delay of 60 seconds between each user to comply with API rate limits


if __name__ == "__main__":
    main()
