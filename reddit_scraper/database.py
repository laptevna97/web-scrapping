import os
import sqlite3
from datetime import datetime

current_dir = os.path.dirname(os.path.abspath(__file__))

DATABASE_FILE = os.path.join(current_dir, "collected_data/database/reddit_users.db")


def create_database():
    """
    Creates the SQLite database if it doesn't exist or connects to an existing database.
    """
    try:
        if not os.path.exists(DATABASE_FILE):
            database_dir = os.path.dirname(DATABASE_FILE)
            if not os.path.exists(database_dir):
                os.makedirs(database_dir)  # Create the directory if it doesn't exist

            conn = sqlite3.connect(DATABASE_FILE)
            cursor = conn.cursor()
            cursor.execute('''CREATE TABLE IF NOT EXISTS user_info (
                                id INTEGER PRIMARY KEY AUTOINCREMENT,
                                username TEXT,
                                date_joined TEXT,
                                reddit_karma INTEGER,
                                subreddits TEXT,
                                average_post_engagement REAL,
                                average_comment_engagement REAL,
                                scraped_at TEXT
                            )''')
            conn.commit()
            print("New database created successfully.")
        else:
            conn = sqlite3.connect(DATABASE_FILE)
            print("Connected to existing database.")
    except sqlite3.Error as e:
        print("Error accessing database:", e)
    finally:
        if conn:
            conn.close()



def insert_user_data(user_info):
    """
    Inserts user data into the database.
    """
    conn = None
    try:
        conn = sqlite3.connect(DATABASE_FILE)
        cursor = conn.cursor()
        cursor.execute('''INSERT INTO user_info (username, date_joined, reddit_karma, subreddits, 
                        average_post_engagement, average_comment_engagement, scraped_at) 
                        VALUES (?, ?, ?, ?, ?, ?, ?)''',
                       (user_info['Username'], user_info['Date Joined'], user_info['Reddit Karma'],
                        ', '.join(user_info['Subreddits the user is active in']),
                        user_info['Average Post Engagement'], user_info['Average Comment Engagement'],
                        datetime.now().strftime('%Y-%m-%d %H:%M:%S')))  # Insert current datetime
        conn.commit()
        print(f"User data for '{user_info['Username']}' inserted successfully.")
    except sqlite3.Error as e:
        print("Error inserting user data:", e)
    finally:
        if conn:
            conn.close()


def query_user_data(username):
    """
    Queries user data from the database.
    """
    conn = None
    try:
        conn = sqlite3.connect(DATABASE_FILE)
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM user_info WHERE username=?", (username,))
        user_data = cursor.fetchone()
        return user_data
    except sqlite3.Error as e:
        print("Error querying user data:", e)
    finally:
        if conn:
            conn.close()
