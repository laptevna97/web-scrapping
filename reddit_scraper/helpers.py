import csv
import os
from datetime import datetime

from docxtpl import DocxTemplate

current_dir = os.path.dirname(os.path.abspath(__file__))
output_folder = os.path.join(current_dir, "collected_data")


def generate_overview_document(user_info: dict, username: str):
    """
    Generates an overview document describing the scraping results.
    """
    template_path = os.path.join(output_folder, "overview_template.docx")
    doc = DocxTemplate(template_path)

    context = {
        "username": user_info['Username'],
        "date_joined": user_info['Date Joined'],
        "reddit_karma": user_info['Reddit Karma'],
        "subreddits": user_info.get("Subreddits the user is active in", []),
        "last_50_posts": user_info.get("Last 50 Posts", []),
        "last_50_comments": user_info.get("Last 50 Comments", []),
        "average_post_engagement": user_info['Average Post Engagement'],
        "average_comment_engagement": user_info['Average Comment Engagement']
    }

    timestamp = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
    output_filename = f"{username}_overview({timestamp}).docx"
    output_path = os.path.join(output_folder, output_filename)
    doc.render(context)
    doc.save(output_path)

    print(f"Overview document saved at: {output_path}")

def save_to_csv(user_info: dict, username: str) -> None:
    """
    Saves the scraping results to a CSV file.
    """
    fields = list(user_info.keys())
    timestamp = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
    output_path = os.path.join(output_folder, f"{username}_scraping_results({timestamp}).csv")
    with open(output_path, "w", newline="", encoding="utf-8") as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=fields)
        writer.writeheader()
        writer.writerow(user_info)
    print(f"CSV document saved at: {output_path}")